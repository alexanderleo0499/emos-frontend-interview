
import { useState } from "react";
import Song from "./components/Song";
import { shuffleArray } from "./utils/index";

const playlist = [
  {
    id: "x",
    title: "Hey",
    artist: "john doe"
  },
  {
    id: "y",
    title: "Hello",
    artist: "jane doe"
  },
  {
    id: "z",
    title: "Woo",
    artist: "jake doe"
  }
];

//ga bole pilih lagu yang sama untuk next play
//next & prev button sesuai dengan behaviour shuffle
//misal belom ada play sama sekali - start random
//misal uda ada yg di play reset dari awal

export default function App() {
  const [playing, setPlaying] = useState();
  const [livedPlaylist, setLivedPlaylist] = useState(playlist);

  const handlePlay = (song) => {
    setPlaying(song);
  };

  //type = next | prev
  const handleNextPrev = (type) => {
    if (!playing || !type) return;
    const idx = livedPlaylist.findIndex((p) => p.id === playing.id);

    let updatedIdx = idx;
    const maxIdx = livedPlaylist.length - 1;
    if (type === "next") {
      updatedIdx = maxIdx === idx ? 0 : idx + 1;
    } else if (type === "prev") {
      updatedIdx = idx === 0 ? maxIdx : idx - 1;
    }
    setPlaying(livedPlaylist[updatedIdx]);
  };

  const handleShufflePlaylist = () => {
    const shuffledArr = shuffleArray(playlist);
    setLivedPlaylist(shuffledArr);
    setPlaying(shuffledArr[0]);
  };

  return (
    <div className="container">
      <div className="font-bold text-sm text-center">
        {playing ? "Now Playing" : "Pick a Song"}
      </div>
      <div className="container-action">
        <button
          className="btn action__btn"
          onClick={() => handleNextPrev("prev")}
        >
          Prev
        </button>
        <div className="playing ">
          <div>
            <div className="font-bold">{playing ? playing.title : "-"}</div>
            <div className="text-sm">{playing ? playing.artist : "-"}</div>
          </div>
        </div>
        <button
          className="btn  action__btn"
          onClick={() => handleNextPrev("next")}
        >
          Next
        </button>
      </div>

      <div className="container-shuffle">
        <button className="btn btn-shuffle" onClick={handleShufflePlaylist}>
          Shuffle Song
        </button>
      </div>
      <div className="container-song">
        {playlist.map((p) => {
          return <Song key={p.id} song={p} onClick={() => handlePlay(p)} />;
        })}
      </div>
    </div>
  );
}
