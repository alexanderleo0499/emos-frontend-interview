export function shuffleArray(array) {
    const copiedArr = [...array];
    for (let i = copiedArr.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [copiedArr[i], copiedArr[j]] = [copiedArr[j], copiedArr[i]];
    }
    return copiedArr;
  }
  