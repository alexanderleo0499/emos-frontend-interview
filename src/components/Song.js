const Song = ({ song, onClick }) => {
    return (
      <div className="mr-1 song">
        <button className="song__btn btn text-sm" onClick={onClick}>
          Play
        </button>
        <div className="song__content">
          <div className="font-bold">{song.title}</div>
          <div className="text-sm">{song.artist}</div>
        </div>
      </div>
    );
  };
  
  export default Song;
  